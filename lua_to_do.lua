

to_do_items_table = {}
DEBUG = false

-- print a a bunch of empty lines to clear the screen
function clear_screen()
  io.write("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
  io.flush()
end

-------------------------------------------------------------------------------

function output_items(items)
  print("\n********** Current To Do Items **********\n")
  for k,v in pairs(items) do
    print("#" .. k .. ": " .. v)
  end
  print("\n*****************************************")
end

-------------------------------------------------------------------------------

todo_file = "todo.txt"
myfile = io.open(todo_file, "r")


-- retrieve to do items from table
function read_items_from_file()
  items = {}
  index = 1
  while true do
    -- print("GOT HERE 1")
    line = myfile:read("*l")
    -- print("GOT HERE 2")
    if line == nil then
      -- print("line is nil, ending!") -- debugging
      break
    end

    if DEBUG then
      print("#" .. index .. ": " .. line) 
    end

    items[index] = line
    index = index + 1
  end

  return items
end



-- user wants to delete a to do item
function delete_item()
  io.write("\nEnter number of to-do item to delete:  ")
  io.flush()
  item_num = io.read()
  table.remove(to_do_items_table, tonumber(item_num))
end



function table_size(current_table)
  num = 0

  for k,v in pairs(current_table) do
    num = num + 1
  end 

  return num
end



-- save table back to file
function save(items_table, default_output_filename)
  local output_filename = default_output_filename

  io.write("\nEnter filename to save items (default is '" .. output_filename .. "'):  ")
  io.flush()
  local user_fname = io.read()
  local fname_size = string.len(user_fname)

  -- manual entry of filename must be at least 2 chars
  if fname_size >= 2 then
    output_filename = user_fname
  end


  io.write("  Saved to file: " .. output_filename .. "\n\n")
  io.flush()
  local f = assert(io.open(output_filename, "w"))

  -- save to-do items to file
  for k,v in pairs(items_table) do
    f:write(v)
    f:write("\n")
  end

  io.close(f)
end



-- user wants to add an item to to do list
function add_item()
  -- num_items = table.getn(to_do_items_table)
  num_items = table_size(to_do_items_table)
  print("Number of items before adding: " .. num_items)
  io.write("Please enter a new item/message to add to the ")
  io.write("\n  to-do list:  ")
  io.flush()
  new_item = io.read()
  new_position = num_items + 1 
  to_do_items_table[new_position] = new_item
end



function show_commands()
  -- give user options
  io.write("\nEnter a command ...\n")
  io.write("  (a)add\n")
  io.write("  (d)elete\n")
  io.write("  (r)efresh\n")
  io.write("  (s)ave\n")
  io.write("  (q)quit\n")
  io.write("\n\n")
end



to_do_items_table = read_items_from_file()

-- output_items(to_do_items_table)

-- io.read()

clear_screen()


user_input = "0"
repeat

  output_items(to_do_items_table)
  io.flush()

  show_commands()
  io.flush()

  -- get input from user
  io.write("\n\n")
  io.flush()
  user_input = io.read()
  clear_screen()
  io.flush()

  -- io.write("\nYou entered=" .. user_input .. "\n")

  -- process user input
  if user_input == "r" then
    -- output_items(to_do_items_table)
  elseif user_input == "a" then
    add_item()
  elseif user_input == "d" then
    io.write("\nDelete ...")
    -- output_items(to_do_items_table)
    delete_item()
    -- output_items(to_do_items_table)
  elseif user_input == "s" then
    output_filename = "todo_out.txt"
    save(to_do_items_table, output_filename)
  elseif user_input == "q" then
    io.write("\nQuitting ...")
  else
    io.write("\n##### Error: unknown command: " .. user_input .. ".  Please try again. #####\n")
  end

  io.flush()   -- ensure latest output is written to screen
until user_input == "q"


-- finish up
io.write("\nBye bye.\n\n")
io.flush()


